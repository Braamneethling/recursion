﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Recursion
{
    [TestFixture]
    public class TestRecursion
    {
        [Test]
        public void Recursion_GivenValue1_PlusValue1_ShouldReturn2()
        {
            //Arrange
            var expected = 2;
            //Act
            var result = Program.CalculateSumRecursively(1, 1);
            //Assert
            Assert.AreEqual(expected, result);
        }
    }
}
